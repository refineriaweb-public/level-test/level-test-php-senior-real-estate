# RW Real Estate
> Real State Package

## Add repository
```
"require": {
    ...
    "refineriaweb-public/level-test-php-senior-real-estate": "1.*"
},
...
"repositories": [
    ...
    {
        "type": "vcs",
        "url": "https://gitlab.com/refineriaweb-public/level-test/level-test-php-senior-real-estate.git"
    }
]
```

## Seeds

### Option A
``` shell script
$ php artisan rw-real-estate.tables:seed
```

### Option B
``` shell script
$ php artisan db:seed  --class="RefineriaWeb\\RWRealEstate\\Database\\Seeds\\DatabaseSeeder"
```

## Public view
```
$ php artisan vendor:publish --provider="RefineriaWeb\RWRealEstate\RWRealEstateServiceProvider"
```

### Test views
Add this routes inside file `routes/web.php`
``` php
Route::get('/home-test-view', function () {
    return view('vendor.rw-real-estate.home');
});

Route::get('/properties-test-view', function () {
    return view('vendor.rw-real-estate.properties');
});
```
