<?php

namespace RefineriaWeb\RWRealEstate\Database\Seeds;

use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 * @package RefineriaWeb\RWRealEstate\Database\Seeds
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(AgentsSeeder::class);
         $this->call(PropertiesTypesSeeder::class);
         $this->call(LocationsSeeder::class);
         $this->call(PropertiesSeeder::class);
    }
}
